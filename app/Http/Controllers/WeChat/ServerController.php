<?php

namespace App\Http\Controllers\WeChat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use EasyWeChat\Foundation\Application;
class ServerController extends Controller
{
    public function server(Request $request,$id){

        $config = config('wechat.'.$id);
        $app = new Application($config);
        $server = $app->server;
        $server->setMessageHandler(function ($message) {
           return  $message->FromUserName; // 用户的 openid
            // $message->MsgType // 消息类型：event, text....
        });
        $response = $server->serve();
        return $response;
    }
}
